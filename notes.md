# Formation yeoman
Pour NT2



## Préambule

## Qui suis-je

## NT2

## La SQIL



## Introduction

## Plan de cours

## Pour qui

## Notions requises

## Quand l'utiliser

## Courte intro aux logiciels libres



## Les outils

## yo

## grunt

## gulp

## npm

## bower

## votre OS préféré

## votre fureteur préféré

## votre éditeur préféré



## Programmeurs, programmez vos tâches répétitives

## CTRL-F5

## Concaténer les CSS et JavaScript

## Minifier les CSS et JavaScript

## Etc.



## Les générateurs yeoman

## Générateurs officiels (15-20)

## Générateurs contribués (env. 1000)

## Des exemples de générateurs

* un site web statique ou dynamique
* une application mobile avec cordova/phonegap
* une application html5, des slides (ceci)
* un thème drupal
* une installation de WordPress
* un générateur yeoman
* etc.
