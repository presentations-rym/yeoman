/*
 * Create missing slide files in ./slides/
 * based on ./slides/list.json
 * Missing or empty slide files are created with a title based on their file name
 * File contents are hard-coded in the writeFile() function
 * and support markdown and html, based on file name extension
 *
 * Run with: node (or nodejs) create-missing.js
 * Not at all integrated with grunt or the yeoman generator (yet)
 */ 

(function() {
    'use strict';

    var fs = require('fs'),
        path = require('path');

    // basé sur grunt.file
    // Read a file, return its contents.
    function read(filepath) {
      try {
        return fs.readFileSync(filepath, 'utf8');
      } catch(e) {
        throw {a:'Unable to read "' + filepath + '" file (Error code: ' + e.code + ').', b:e};
      }
    };

    // basé sur grunt.file
    // Read a file, parse its contents, return an object.
    function readJSON(filepath) {
      var src = read(filepath);
      
      try {
        return JSON.parse(src);
      } catch(e) {
        throw {a:'Unable to parse "' + filepath + '" file (Error message: ' + e.message + ').', b:e};
      }
    };

    function readList(filepath) {
        var content = readJSON('slides/list.json'),
            section, subsection, filenames = {}, ret = [], i;

        for (section = 0; section < content.length; ++section) {
            if ('string' === typeof content[section]) {
                filenames[content[section]] = true;
            } else {
                for (subsection = 0; subsection < content[section].length; ++subsection) {
                    if ('string' === typeof content[section][subsection]) {
                        filenames[content[section][subsection]] = true;
                    } else if (content[section][subsection].filename) {
                        filenames[content[section][subsection].filename] = true;
                    }
                }
            }
        }

        for (i in filenames) {
            ret.push('slides/' + i);
        }

        return ret;
    }

    function writeFile(fd) {
        // nom du fichier selon le file descriptor
        fs.readlink('/proc/self/fd/' + fd, function(err, fn) {
            var str, buffer,
                extension = path.extname(fn),
                // str.replace(...) ne remplace que la première occurence
                bn = path.basename(fn, extension).split('-').join(' ');
            
            if ('.html' === extension) {
                str = '<h2>' + bn + '</h2>\n\n<p>Bla</p>\n\n<aside class="notes">\nPut your speaker notes here. You can see them pressing "s".\n</aside>\n';
            } else if ('.md' === extension) {
                str = '## ' + bn + '\n\nBlabla\n\nnote:\nPut your speaker notes here. You can see them pressing "s".\n';
            } else {
                console.log("format de fichier inconnu: " + fn);
                return;
            }

            buffer = new Buffer(str);
            fs.write(fd, buffer, 0, buffer.length, null, function(err, written, buffer) {
                fs.close(fd, function(err) {
                    if (err) {
                        console.log('erreur avec ' + fn);
                        console.log(err);
                    } else {
                        console.log(fn + ': créé.');
                    }
                });
            });
        });
    }

    function createIf(fn, flags) {
        // on crée un fichier s'il n'existe pas
        fs.open(fn, flags || 'wx', function(err, fd) {
            if (err) {
                // il existe déjà? on vérifie s'il est vide
                fs.stat(err.path, function(err, stats) {
                    if (stats.size) {
//                        console.log(fn + ': le fichier existe et n\'est pas vide');
                    } else {
                        // il est vide, mettons-y du contenu
                        createIf(fn, 'w');
                    }
                });
            } else {
                writeFile(fd);
            }
        });
    }
    

    function createIfMissing(filepath) {
        var r, filenames = readList(filepath);
        
        for (r = 0; r < filenames.length; ++r) {
            createIf(filenames[r]);
        }
    }

    console.log('hop');
    createIfMissing('slides/list.json');
}());
