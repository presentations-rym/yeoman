# Cours sur Yeoman de 7h initialement dans le cadre des formations du laboratoire NT2

Voir slides/list.json pour les grands titres du contenu.

Cette formation dépend de yeoman pour sa construction et de reveal.js pour sa présentation.

Après un git clone, on procède ainsi pour lancer un serveur web local et montrer/éditer la présentation:

```bash
npm install # installe les dépendances nodejs (le serveur web, outils de minification, etc.)
bower install # installe reveal.js côté client et le reste du javascript et css client
grunt # lance les tests et le serveur web à http://localhost:9000
```

Le projet va occuper environ 30 MiB d'espace disque. Ça inclut tous les logiciels nécessaires à maintenant la présentation.

## Licence
Creative Commons Attribution (BY) 4.0
