## Cette présentation

* créer ave le générateur yeoman pour reveal.js
* reveal.js lui-même est un outil JavaScript/CSS côté client
* ajout d'un petit script pour générer les diapo à partir de list.json
* https://github.com/slara/generator-reveal

```bash
sudo npm -g generator-reveal
```

note:
Put your speaker notes here. You can see them pressing "s".
