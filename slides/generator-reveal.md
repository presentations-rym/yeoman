## Reveal.js generator

https://github.com/slara/generator-reveal

```bash
sudo npm -g generator-reveal # installer le générateur globablement
mkdir my-new-project && cd $_ # on crée le répertoire du projet
# et s'y déplacer
yo reveal # créer le projet en répondant à quelques questions
grunt # lancer le serveur web local sur http://localhost:9000
yo reveal:slide "Titre de la diapo" # html
yo reveal:slide "Titre de la diapo #2" --markdown
```

note:
Put your speaker notes here. You can see them pressing "s".
