## yo reveal:slide "Titre diapo"

Une fois qu'on a démarré le projet avec <code>yo reveal<code>, on peut créer les diapos en répétant cette commande:

```bash
yo reveal:slide "Titre de la diapo" # html
# ou pour faire du markdown:
yo reveal:slide "Titre de la diapo #2" --markdown
```

Les références aux diapos (noms de fichiers) s'ajoutent automatiquement dans slides/list.json

note:
Put your speaker notes here. You can see them pressing "s".
